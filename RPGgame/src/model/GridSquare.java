/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 * A class for representing a grid square in the dungeon
 * @author Roman
 */
public class GridSquare {

    private Occupant occupant;
    private Player player;
    private FloorInterior floorInterior;
    private static int MAX_OCCUPANTS = 3;
    private ArrayList<Occupant> occupants;
    private FloorInterior interior;

    /**
     * creates a new instance of a grid square
     */
    public GridSquare() {
        occupant = null;
        this.floorInterior = FloorInterior.WALKABLE;
        this.occupants = new ArrayList<Occupant>();
    }

    /**
     * floor Interior getter
     * @return floorInterior
     */
    public  FloorInterior getFloorInterior() {
        return floorInterior;
    }

    /**
     * floor Interior setter
     * @param floorInterior
     */
    public void setFloorInterior(FloorInterior floorInterior) {
        this.floorInterior = floorInterior;
    }

    /**
     * string representation getter
     * @return string representation
     */
    public String getFloorStringRepresentation() {
        String result = floorInterior.getStringRepresentation();
        return result;
    }

    /**
     *  return a string representation of the occupants in the grid square
     * @return string representation
     */
    public String getOccupantStringRepresentation() {
        String result = "";
        for (Occupant occupant : occupants) {
            result += occupant.getStringRepresentation();
        }

        return result;
    }

    /**
     *  checks if the grid square is occupied
     * @return true if the grid square is occupied
     */
    public boolean hasOccupant() {
        return occupant != null;
    }

    /**
     *  Checks if an occupant is in this grid square
     * @return the player is in this grid square
     */
    public boolean hasPlayer() {

        return (this.player != null);
    }

    /** 
     * Player setter
     * @param player
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * returns a occupant in the grid square
     * @return occupants array
     */
    public Occupant[] getOccupants() {

        return occupants.toArray(new Occupant[occupants.size()]);
    }

    /**
     * add a new occupant to the grid square
     * @param newOcc
     * @return true if an occupant was added successfully
     */
    public boolean addOccupant(Occupant newOcc) {
        boolean success = false;
        if ((newOcc != null) && !hasOccupant()) {
            occupants.add(newOcc);
            success = true;
        }

        return success;
    }

    /**
     * removes the specified occupant from the grid square
     * @param occupant
     * @return if the occupant was removed successfully
     */
    public boolean removeOccupant(Occupant occupant) {
        boolean success = false;
        boolean validOccupant = occupant != null;
        if (validOccupant) {
            success = occupants.remove(occupant);
        }
        return success;
    }

    /**
     * @return the interior
     */
    public FloorInterior getInterior() {
        return interior;
    }

    /**
     * @param interior the interior to set
     */
    public void setInterior(FloorInterior interior) {
        this.interior = interior;
    }

    /**
     * Checks if there are any enemies in the grid square
     * @return number of enemies in the grid square
     */
    public int checkEnemies() {
        int enemiesCount = 0;
        for (Occupant occ : occupants) {
            if (occ instanceof Enemy) {
                Enemy en =(Enemy) occ;
                if(!en.isIsDead())
                enemiesCount += 1;
            }

        }

        return enemiesCount;
    }

    /**
     * returns a image path representing multiple enemies in the grid square
     * @return an image path for the enemies in the grid square
     */
    public String getEnemyImagePath() {
        String imagePath = "images/enemies/group.png";
        if (checkEnemies() == 1) {
            Enemy en = (Enemy) occupants.get(0);
            imagePath = en.getImagePath();

        }
        return imagePath;
    }

    /**
     * counts number of items in the grid square
     * @return number of items in the grid square
     */
    public int checkItems() {
        int itemsCount = 0;
        for (Occupant occ : occupants) {
            if (occ instanceof Item) {
                itemsCount += 1;
            }

        }

        return itemsCount;
    }

    /**
     * returns a image path representing multiple items in the grid square
     * @return an image path for the items in the grid square
     */
    public String getItemImagePath() {
        String imagePath = "images/items/chest.png";
        if (checkItems() == 1) {
            Item item = (Item) occupants.get(0);
            imagePath = item.getImagePath();

        }
        return imagePath;
    }

}
