/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * a class that represents an item
 * @author Roman
 */
public class Item extends Occupant {

    private double health;
    private double mana;
    private int strength;
    private int vitality;
    private int speed;
    private int intelligence;
    private double attack;
    private double defence;
    private String imagePath;
    private String itemType;
    private boolean itemEquipped = false;

    /**
     *  creates a new instance of an item
     * @param pos position of the item
     * @param name name of the item
     * @param description description of the item
     */
    public Item(Position pos, String name, String description) {
        super(pos, name, description);
        health = 0;
        mana = 0;
        strength = 0;
    }

  

    /**
     * health getter
     * @return health
     */
    public double getHealth() {
        return health;
    }

    /**
     *mana getter
     * @return mana
     */
    public double getMana() {
        return mana;
    }

    /**
     * attack getter
     * @return attack
     */
    public double getAttack() {
        return attack;
    }

    /**
     * defence getter
     * @return defence
     */
    public double getDefence() {
        return defence;
    }



    @Override
    public String getStringRepresentation() {
        return "I";
    }

    @Override
    public String toString() {
        return name + " Description: Health + " + getHealth();
    }

    /**
     * @param health the health to set
     */
    public void setHealth(double health) {
        this.health = health;
    }

    /**
     * @param mana the mana to set
     */
    public void setMana(double mana) {
        this.mana = mana;
    }

    /**
     * @return the strength
     */
    public int getStrength() {
        return strength;
    }

    /**
     * @param strength the strength to set
     */
    public void setStrength(int strength) {
        this.strength = strength;
    }

    /**
     * @return the vitality
     */
    public int getVitality() {
        return vitality;
    }

    /**
     * @param vitality the vitality to set
     */
    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    /**
     * @return the speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * @return the intelligence
     */
    public int getIntelligence() {
        return intelligence;
    }

    /**
     * @param intelligence the intelligence to set
     */
    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    /**
     * @param attack the attack to set
     */
    public void setAttack(double attack) {
        this.attack = attack;
    }

    /**
     * @param defence the defence to set
     */
    public void setDefence(double defence) {
        this.defence = defence;
    }

    /**
     * @return the imagePath
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * @param imagePath the imagePath to set
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    /**
     * @return the itemType
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * @param itemType the itemType to set
     */
    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    /**
     * @return the itemEquipped
     */
    public boolean isItemEquipped() {
        return itemEquipped;
    }

    /**
     * @param itemEquipped the itemEquipped to set
     */
    public void setItemEquipped(boolean itemEquipped) {
        this.itemEquipped = itemEquipped;
    }
}
