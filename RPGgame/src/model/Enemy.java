/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * a class representing an enemy in the dungeon
 * @author Roman
 */
public class Enemy extends Occupant {

    private int strength;
    private int vitality;
    private int speed;
    private int intelligence;
    private double health;
    private double attack;
    private double defence;
    
    private int enemyID;
    private String imagePath;
    private boolean isDead = false;
    public Enemy(Position pos, String name, String description) {
        super(pos, name, description);
    }

    /**
     * attack getter
     * @return attack
     */
    public double getAttack() {
        return attack;
    }

    /** defence getter
     *
     * @return defence
     */
    public double getDefence() {
        return defence;
    }

    /**
     * strength getter
     * @return strength
     */
    public int getStrength() {
        return strength;
    }

    /**
     * vitality getter
     * @return vitality
     */
    public int getVitality() {
        return vitality;
    }

    /**
     * speed getter
     * @return speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * intelligence getter
     * @return intelligence
     */
    public int getIntelligence() {
        return intelligence;
    }

    /**
     * health getter
     * @return health
     */
    public double getHealth() {
        return health;
    }



    @Override
    public String getStringRepresentation() {
        String output="";
        if(!isDead)
            output="E";
        return output;
    }

    /**
     * @param strength the strength to set
     */
    public void setStrength(int strength) {
        this.strength = strength;
    }

    /**
     * @param vitality the vitality to set
     */
    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * @param intelligence the intelligence to set
     */
    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    /**
     * @param health the health to set
     */
    public void setHealth(double health) {
        
        this.health = health;
        if(this.health<0)
            setIsDead(true);
    }

    /**
     * @param attack the attack to set
     */
    public void setAttack(double attack) {
        this.attack = attack;
    }

    /**
     * @param defence the defence to set
     */
    public void setDefence(double defence) {
        this.defence = defence;
    }

    /**
     * @return the imagePath
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * @param imagePath the imagePath to set
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    /**
     * @return the enemyID
     */
    public int getEnemyID() {
        return enemyID;
    }

    /**
     * @param enemyID the enemyID to set
     */
    public void setEnemyID(int enemyID) {
        this.enemyID = enemyID;
    }

    /**
     * @return the isDead
     */
    public boolean isIsDead() {
        return isDead;
    }

    /**
     * @param isDead the isDead to set
     */
    public void setIsDead(boolean isDead) {
        this.isDead = isDead;
    }
}
