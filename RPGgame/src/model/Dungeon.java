/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * a class for representing a dungeon
 * @author Roman
 */
public class Dungeon {

    private int numRows;
    private int numColumns;
    private FloorInterior floorInterior;
    private GridSquare[][] dungeonGrid;
    private Position previousPlayerPos;

    /**
     * create a new instance of the dungeon
     * @param numRows
     * @param numColumns
     */
    public Dungeon(int numRows, int numColumns) {
        System.err.println("Creating dungeon " + numRows + "x" + numColumns);
        this.numRows = numRows;
        this.numColumns = numColumns;
        this.previousPlayerPos = null;
        createDungeonGrid();

    }

    /**
     * number of rows getter
     * @return
     */
    public int getNumRows() {
        return this.numRows;
    }

    /**
     * number of columns getter
     * @return numColumn
     */
    public int getNumColumns() {
        return this.numColumns;
    }

    /**
     * returns the grid square depending on the provided position
     * @param pos position of the grid square
     * @return grid square
     */
    public GridSquare getGridSquare(Position pos) {
        GridSquare result = null;
        if (pos != null) {
            if (pos.isPresent()) {
                result = dungeonGrid[pos.getRow()][pos.getColumn()];
            }
        }
        return result;
    }

    /**
     * return the grid square from the provided x and y
     * @param x x position of the grid square
     * @param y y position of the grid square
     * @return grid square
     */
    public GridSquare getGridSquareByNumbers(int x, int y) {
        GridSquare result;
        result = dungeonGrid[x][y];
        return result;
    }

    private void createDungeonGrid() {
        System.err.println("Creating dungeon grid");
        dungeonGrid = new GridSquare[numRows][numColumns];
        for (int row = 0; row < numRows; row++) {
            for (int column = 0; column < numColumns; column++) {
                GridSquare gs = new GridSquare();
                dungeonGrid[row][column] = gs;
            }
        }
    }

    /**
     * Draws the dungeon
     */
    public void draw() {
        final int CELL_SIZE = 4;

        // create the horizontal line as a string
        String horizontalLine = "-";
        for (int col = 0; col < this.numColumns; col++) {
            for (int i = 0; i < CELL_SIZE; i++) {
                horizontalLine += "-";
            }
            horizontalLine += "-";
        }

        // print the content
        for (int row = 0; row < this.numRows; row++) {
            String rowOccupant = "|";
            String rowFloor = "|";
            for (int col = 0; col < this.numColumns; col++) {
                GridSquare g = dungeonGrid[row][col];
                // create string with occupants
                String cellOccupant = "";
                if (g.hasPlayer()) {
                    cellOccupant += "P";
                }

                cellOccupant += g.getOccupantStringRepresentation();
                for (int i = cellOccupant.length(); i < CELL_SIZE; i++) {
                    cellOccupant += " ";
                }

                rowOccupant += cellOccupant + "|";
                // create string with floor
                String cellFloor = "";
                for (int i = 0; i < CELL_SIZE; i++) {
                    cellFloor += g.getFloorStringRepresentation();
                }
                rowFloor += cellFloor + "|";
            }
            System.out.println(horizontalLine);
            System.out.println(rowOccupant);
            System.out.println(rowFloor);
        }
        System.out.println(horizontalLine);
    }

    /**
     * updates players position with 
     * @param player player to update position for
     */
    public void updatePlayerPosition(Player player) {
        Position pos = player.getPosition();
        getGridSquare(pos).setPlayer(player);
        if (previousPlayerPos != null) {
            getGridSquare(previousPlayerPos).setPlayer(null);
        }
    }

    /**
     * adds the provided occupant in the provided position in the dungeon
     * @param pos position of the occupant
     * @param occupant the occupant to add
     * @return true if the occupant was added successfully
     */
    public boolean addOccupant(Position pos, Occupant occupant) {
        boolean success = false;
        if (pos.isPresent() && occupant != null) {
            GridSquare gs = getGridSquare(pos);
            success = gs.addOccupant(occupant);
        }
        if (success) {
            //update the occupants address
            occupant.setPosition(pos);
        }
        return success;
    }

    /**
     * removes the provided occupant from the dungeon in the provided position
     * @param pos position 
     * @param occupant the occupant to remove
     * @return  true if removal was successful
     */
    public boolean removeOccupant(Position pos, Occupant occupant) {
        boolean success = false;
        if (pos.isPresent() && occupant != null) {
            GridSquare gs = getGridSquare(pos);
            success = gs.removeOccupant(occupant);
        }
        if (success) {
            //update the occupants address to the "not on island position"
            occupant.setPosition(Position.NotPresent);
        }
        return success;
    }

    /**
     * @param previousPlayerPos the previousPlayerPos to set
     */
    public void setPreviousPlayerPos(Position previousPlayerPos) {
        this.previousPlayerPos = previousPlayerPos;
    }

}
