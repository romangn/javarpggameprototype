/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashSet;
import java.util.Set;

/**
 * a class that represents player
 *
 * @author Roman
 */
public class Player {

    private double maxHealth;
    private double maxMana;
    private Position position;
    private String name;
    private String className;
    private int strength;
    private int vitality;
    private int speed;
    private int intelligence;
    private double health;
    private double mana;
    private double defence;
    private double attack;
    private double damage;
    private Set<Item> inventory;
    private Set<Item> equippedItems;
    private boolean armorEquipped = false;
    private boolean weaponEquipped = false;
    private boolean isDead = false;

    /**
     * creates a new instance of the player
     */
    public Player() {
        this.inventory = new HashSet<Item>();
        this.equippedItems = new HashSet<Item>();
    }

    /**
     * Sets warrior parametres for the player
     */
    public void setWarriorAttributes() {
        className = "Warrior";
        setStrength(10);
        setVitality(8);
        setSpeed(6);
        setIntelligence(5);
        setHealth(100);
        setMana(15);
        setAttack(10.0);
        setDefence(7.0);
        maxHealth = 100;
        maxMana = 15;
    }

    /**
     * Sets mage parametres for the player
     */
    public void setMageAttributes() {
        className = "Mage";
        setStrength(3);
        setVitality(5);
        setSpeed(4);
        setIntelligence(10);
        setHealth(50);
        setMana(60);
        setAttack(3.0);
        setDefence(3.0);
        maxHealth = 50;
        maxMana = 60;
    }

    /**
     * Sets assasin parametres for the player
     */
    public void setAssasinAttributes() {
        className = "Assasin";
        setStrength(6);
        setVitality(4);
        setSpeed(10);
        setIntelligence(6);
        setHealth(70);
        setMana(20);
        setAttack(7.0);
        setDefence(4.0);
        maxHealth = 70;
        maxMana = 20;
    }

    /**
     * position getter
     *
     * @return position
     */
    public Position getPosition() {
        return position;
    }

    /**
     * attack getter
     *
     * @return attack
     */
    public double getAttack() {
        return attack;
    }

    /**
     * defence getter
     *
     * @return defence
     */
    public double getDefence() {
        return defence;
    }

    /**
     *
     * @return
     */
    public int getStrength() {
        return strength;
    }

    /**
     *
     * @return
     */
    public int getVitality() {
        return vitality;
    }

    /**
     *
     * @return
     */
    public int getSpeed() {
        return speed;
    }

    /**
     *
     * @return
     */
    public int getIntelligence() {
        return intelligence;
    }

    /**
     *
     * @return
     */
    public double getHealth() {
        return health;
    }

    /**
     *
     * @return
     */
    public double getmana() {
        return getMana();
    }

    /**
     *
     * @return
     */
    public double getDamage() {
        return damage;
    }

    /**
     * Prints player's stats
     */
    public void printStats() {
        System.out.println("Stats are: ");
        System.out.println("Character name is " + getName());
        System.out.println("Your class is " + className);
        System.out.println("Strength is " + getStrength());
        System.out.println("Vitality is " + getVitality());
        System.out.println("Speed is " + getSpeed());
        System.out.println("Intelligence is " + getIntelligence());
        System.out.println("Attack is " + getAttack());
        System.out.println("Defence is " + getDefence());
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * returns a string represention for the player
     *
     * @return string represention for the player
     */
    public String getStringRepresentaion() {
        return "P";
    }

    /**
     * Position setter
     *
     * @param pos
     */
    public void setPosition(Position pos) {

        this.position = pos;
    }

    /**
     * moves player to the specified position
     *
     * @param pos
     */
    public void moveToPosition(Position pos) {
        this.position = pos;
    }

    /**
     * pick ups specified item in the specified grid square
     * @param item to collect
     * @param sq grid square where item is located
     * @return true if pick up was successful
     */
    public boolean pickUpItem(Item item, GridSquare sq) {
        boolean itemOK = (item != null);
        boolean success = false;
        if (itemOK) {

            {
                success = getInventory().add(item);

                if (success) {
                    sq.removeOccupant(item);
                    item.setPosition(Position.NotPresent);

                }
            }
        }
        return success;
    }

    /**
     * @return the inventory
     */
    public Set<Item> getInventory() {
        return inventory;
    }

    public boolean isAlive() {
        return getHealth() > 0.0;
    }

    /**
     * @return the maxHealth
     */
    public double getMaxHealth() {
        return maxHealth;
    }

    /**
     * @param maxHealth the maxHealth to set
     */
    public void setMaxHealth(double maxHealth) {
        this.maxHealth = maxHealth;
    }

    /**
     * @return the maxMana
     */
    public double getMaxMana() {
        return maxMana;
    }

    /**
     * @param maxMana the maxMana to set
     */
    public void setMaxMana(double maxMana) {
        this.maxMana = maxMana;
    }

    /**
     * @param strength the strength to set
     */
    public void setStrength(int strength) {
        this.strength = strength;
    }

    /**
     * @param vitality the vitality to set
     */
    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * @param intelligence the intelligence to set
     */
    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    /**
     * @param health the health to set
     */
    public void setHealth(double health) {

        this.health = health;
        if (this.health >= maxHealth) {
            this.health = maxHealth;
        }
        if (this.health < 0) {
            setIsDead(true);
        }

    }

    /**
     * @return the mana
     */
    public double getMana() {
        return mana;
    }

    /**
     * @param mana the mana to set
     */
    public void setMana(double mana) {
        this.mana = mana;

        if (this.mana >= maxMana) {
            this.mana = maxMana;
        }
    }

    /**
     * @param defence the defence to set
     */
    public void setDefence(double defence) {
        this.defence = defence;
    }

    /**
     * @param attack the attack to set
     */
    public void setAttack(double attack) {
        this.attack = attack;
    }

    /**
     * @param damage the damage to set
     */
    public void setDamage(double damage) {
        this.damage = damage;
    }

    /**
     * @return the equippedItems
     */
    public Set<Item> getEquippedItems() {
        return equippedItems;
    }

    /**
     * @param equippedItems the equippedItems to set
     */
    public void setEquippedItems(Set<Item> equippedItems) {
        this.equippedItems = equippedItems;
    }

    /**
     * @return the armorEquipped
     */
    public boolean isArmorEquipped() {
        return armorEquipped;
    }

    /**
     * @param armorEquipped the armorEquipped to set
     */
    public void setArmorEquipped(boolean armorEquipped) {
        this.armorEquipped = armorEquipped;
    }

    /**
     * @return the weaponEquipped
     */
    public boolean isWeaponEquipped() {
        return weaponEquipped;
    }

    /**
     * @param weaponEquipped the weaponEquipped to set
     */
    public void setWeaponEquipped(boolean weaponEquipped) {
        this.weaponEquipped = weaponEquipped;
    }

    /**
     * @return the isDead
     */
    public boolean isIsDead() {
        return isDead;
    }

    /**
     * @param isDead the isDead to set
     */
    public void setIsDead(boolean isDead) {
        this.isDead = isDead;
    }

}
