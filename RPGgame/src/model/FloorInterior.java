/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * A class for representing a floor interior of the grid square in the dungeon
 * @author Roman
 */
public enum FloorInterior {

    WALKABLE("."),
    WALL("*");

    private final String stringRepres;

    private FloorInterior(String stringRepres) {
        this.stringRepres = stringRepres;
    }

    /**
     *
     * @return
     */
    public String getStringRepresentation() {
        return stringRepres;
    }

    /**
     *
     * @param rep
     * @return
     */
    public static FloorInterior getFloorFromStringRepresentation(String rep) {
        FloorInterior ret = null;
        for (FloorInterior t : values()) {
            if (t.getStringRepresentation().equals(rep)) {
                ret = t;
            }
        }
        return ret;
    }

}
