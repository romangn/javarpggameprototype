/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * A class for representing the position of the occupant
 * @author Roman
 */
public class Position {
    
    private Dungeon dungeon;
    private int row;
    private int column;
    public static final Position NotPresent = new Position();
    
    private Position()
    {
        this.dungeon = null;
        this.row    = 0;
        this.column = 0;
    }
    
    /**
     * Creates a new instance of position with provided parametres
     * @param dungeon
     * @param row
     * @param column
     */
    public Position(Dungeon dungeon, int row, int column) {
              if ( dungeon == null )
        {
            throw new IllegalArgumentException(
                    "Dungeon parameter cannot be null");
        }
        if ( (row < 0) || (row >= dungeon.getNumRows()) )
        {
            throw new IllegalArgumentException(
                    "Invalid row for position (" + row + ")");
        }
        if ( (column < 0) || (column >= dungeon.getNumColumns()) )
        {
            throw new IllegalArgumentException(
                    "Invalid column for position (" + column + ")");
        }
        this.dungeon = dungeon;
        this.row = row;
        this.column = column;
    }
    
    /**
     *getter for row field
     * @return row
     */
    public int getRow() {
        return this.row;
    }
    
    /**
     * getter for column field
     * @return column
     */
    public int getColumn() {
        return this.column;
    }
    
    /**
     *removes the position from the dungeon
     */
    public void removeFromDungeon()
    {
        this.dungeon = null;
        this.row    = 0;
        this.column = 0;
    }
        
    /**
     * checks if position is present
     * @return true if position is present
     */
    public boolean isPresent()
    {
        return (this.dungeon != null);
    } 
    
    /**
     * returns a new position based on the provided moving direction
     * @param direction
     * @return new position
     */
    public Position getNewPosition(MoveDirection direction)
    {
        Position newPosition = null;
        if ( isPresent() )
        {
            if ( (direction == MoveDirection.NORTH) && (row > 0) )
            {
                newPosition = new Position(dungeon, row-1, column);
            }
            else if ( (direction == MoveDirection.EAST) &&
                      (column < dungeon.getNumColumns() - 1) )
            {
                newPosition = new Position(dungeon, row, column + 1);
            }
            else if ( (direction == MoveDirection.SOUTH) &&
                      (row < dungeon.getNumRows() -1) )
            {
                newPosition = new Position(dungeon, row+1, column);
            }
            else if ( (direction == MoveDirection.WEST) && (column > 0) )
            {
                newPosition = new Position(dungeon, row, column-1);
            }
        }
        return newPosition;
    }
        
    
    
    
    
}
