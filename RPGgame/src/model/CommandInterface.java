/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import GUI.RpgGUI;
import com.sun.org.apache.xml.internal.serialize.HTMLdtd;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import rpgDataBase.RPGDataBase;

/**
 * this class represents command interface to be used for controlling the game
 *
 * @author Roman
 */
public class CommandInterface {

    private Player player;
    private Dungeon dungeon;
    private boolean playerCreated = false;
    private boolean launchGUI = false;
    private RPGDataBase db;
    private boolean inBattle = false;

    /**
     * a starting  method for controlling the game
     * @throws IOException
     * @throws SQLException
     */
    public void controlProgram() throws IOException, SQLException {

        this.db = new RPGDataBase();
        RpgGUI ui = null;
        setPlayer(new Player());
        createPlayer(true);
        initialiseDungeonFromFile("DungeonData.txt");
        System.out.println("Type GUI for gui interface type commandline for a command line interface");
        runStartingCommand(getInput());
        if (launchGUI == false) {

            System.out.println("Text interface chosen");
            System.out.println("Welcome. Type new game to start new game,qs for quick start, or help"
                    + " to view other available commands.");
            runCommand(getInput());

        } else {

            initialiseDungeonFromFile("DungeonData.txt");
            System.out.println("GUI chosen");
            ui = new RpgGUI(this);
            ui.startGUI();

        }

    }

    /**
     * this method perform a command for choosing user interface
     * specified as a parameter
     * @param command
     * @throws SQLException
     */
    public void runStartingCommand(String command) throws SQLException {
        switch (command) {
            case "GUI":
                launchGUI = true;
                break;
            case "commandline":
                launchGUI = false;
                break;
            default:
                runStartingCommand(getInput());
                break;
        }
    }

    /**
     * resets dungeon status
     * @throws SQLException
     */
    public void resetDungeon() throws SQLException {
        setPlayer(new Player());
        getPlayer().setWarriorAttributes();
        initialiseDungeonFromFile("DungeonData.txt");
    }

    /**
     * this method perform a command which is specified as a parameter
     * specified as a parameter
     * @param command
     * @throws SQLException
     */
    public void runCommand(String command) throws SQLException {

        switch (command) {
            case "quit":
                quitGame();
                break;

            case "new game":
                initialiseDungeonFromFile("DungeonData.txt");
                resetDungeon();
                createPlayer(false);
                break;
            case "view stats":
                if (getPlayer().getName() != null) {
                    getPlayer().printStats();
                } else {
                    System.err.println("Create player first");
                }
                break;
            case "help":
                helpDisplay();
                break;
            case "pick up item":
                pickUpItem();
                getDungeon().draw();
                break;
            case "inventory": {
                Object[] items = getPlayer().getInventory().toArray();
                for (int i = 0; i < items.length; i++) {
                    Item it = (Item) items[i];
                    System.out.println(i + ". " + it.getName() + " " + it.getDescritption());

                }
                String itemCom = getInput();
            }

            case "use item 0": {

                System.out.println(useEquipDeequipItem(0));
            }

            break;
            case "use item 1": {
                System.out.println(useEquipDeequipItem(1));
            }

            break;
            case "use item 2": {
                System.out.println(useEquipDeequipItem(2));
            }

            break;
            case "use item 3": {
                System.out.println(useEquipDeequipItem(3));
            }

            break;
            case "use item 4": {
                System.out.println(useEquipDeequipItem(4));
            }

            break;
            case "use item 5": {
                System.out.println(useEquipDeequipItem(5));
            }

            break;
            case "use item 6": {
                System.out.println(useEquipDeequipItem(6));
            }

            break;

            case "use item 7": {
                System.out.println(useEquipDeequipItem(7));
            }

            break;
            case "move south": {
                movePlayer(MoveDirection.SOUTH);
                dungeon.draw();
                break;
            }
            case "move north": {
                movePlayer(MoveDirection.NORTH);
                dungeon.draw();
                break;
            }
            case "move west": {
                movePlayer(MoveDirection.WEST);
                dungeon.draw();
                break;
            }
            case "move east": {
                movePlayer(MoveDirection.EAST);
                dungeon.draw();
                break;
            }
            case "print hpmana": {

                dungeon.draw();
                System.out.println("HP " + player.getHealth() + "/" + player.getMaxHealth());
                System.out.println("Mana " + player.getMana() + "/" + player.getMaxMana());
                break;
            }
            case "attack": {
                if(!player.isIsDead()) {
                    
                if(getEnemiesNearPlayer().length>0) {
                String firstEnemy = "";
                String secondEnemy = "";
                
                if (getEnemiesNearPlayer()[0] != null && getEnemiesNearPlayer()[0] != null) {
                    Enemy en = (Enemy) getEnemiesNearPlayer()[0];
                    firstEnemy = attackEnemy(en, false) + dealDamageToPlayer(en);

                }
                if (getEnemiesNearPlayer().length > 1) {
                    if (getEnemiesNearPlayer()[1] != null && getEnemiesNearPlayer()[1] != null) {
                        Enemy en = (Enemy) getEnemiesNearPlayer()[1];
                        secondEnemy = attackEnemy(en, false) + dealDamageToPlayer(en);
                    }
                }
                dungeon.draw();
                System.out.println(firstEnemy);
                System.out.println(secondEnemy);
                }
                }
                else 
                    System.out.println("Player is dead");
                break;
            }

            case "magic attack": {
                if(!player.isIsDead()) {
                String firstEnemy = "";
                String secondEnemy = "";
                if(getEnemiesNearPlayer().length>0) {
                if (getEnemiesNearPlayer()[0] != null && getEnemiesNearPlayer()[0] != null) {
                    Enemy en = (Enemy) getEnemiesNearPlayer()[0];
                    firstEnemy = attackEnemy(en, true) + dealDamageToPlayer(en);

                }
                if (getEnemiesNearPlayer().length > 1) {
                    if (getEnemiesNearPlayer()[1] != null && getEnemiesNearPlayer()[1] != null) {
                        Enemy en = (Enemy) getEnemiesNearPlayer()[1];
                        secondEnemy = attackEnemy(en, true) + dealDamageToPlayer(en);
                    }
                }
                dungeon.draw();
                System.out.println(firstEnemy);
                System.out.println(secondEnemy);
                }
            } else
                    System.out.println("Player is dead");
                
                break;
            }

            case "draw":
              
                   
                    getDungeon().draw();


                break;
            case "qs":
                initialiseDungeonFromFile("DungeonData.txt");
                resetDungeon();
                createPlayer(true);
                getDungeon().draw();
                break;
            default:

                break;
        }
        String commands = getInput();

        runCommand(commands);
    }

    /**
     * this method equips, deequips or uses an item based on its number in the 
     * player's inventory
     * @param itemNumber
     * @return the output based on the actions completed
     */
    public String useEquipDeequipItem(int itemNumber) {
        String equipped = "";
        Object[] items = getPlayer().getInventory().toArray();
        Item it = null;
        if (items.length > itemNumber) {
            it = (Item) items[itemNumber];
        }
        if (it != null) {

            char itemType = it.getItemType().charAt(0);
            if (itemType == 'P') {
                useItem(it);
                equipped = it.getName() + " used";
            } else {

                if (it.isItemEquipped()) {
                    deequipArmorWeapon(it);
                    equipped = it.getName() + " deequipped";

                } else {
                    if (itemType == 'A') {
                        equipArmor(it);
                        equipped = it.getName() + " equipped";

                    } else {
                        equipWeapon(it);
                        equipped = it.getName() + " equipped";
                    }

                }
            }

        }

        return equipped;

    }

    /**
     * this method creates a new player
     * @param fastStart true if player should be created  with default 
     * player's attributes
     */
    public void createPlayer(boolean fastStart) {

        boolean classChosen = false;

        String input = "";
        String name = "";
        if (fastStart == false) {
            System.out.println("Character creation!");
            System.out.println("Name your character");
            name = getInput();
            getPlayer().setName(name);
            while (!classChosen) {
                System.out.println("Choose your class warrior, assasin or mage");
                input = getInput();

                switch (input) {
                    case "mage": {
                        getPlayer().setMageAttributes();
                        classChosen = true;
                        break;
                    }
                    case "warrior": {
                        getPlayer().setWarriorAttributes();
                        classChosen = true;
                        break;
                    }
                    case "assasin": {
                        getPlayer().setAssasinAttributes();
                        classChosen = true;
                        break;
                    }

                }
            }
        } else {
            getPlayer().setName("player");
            getPlayer().setWarriorAttributes();
            classChosen = true;
        }
        playerCreated = true;
        System.out.println("All set!");

    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    /**
     * player getter
     * @return player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * item getter
     * @param item
     */
    public void useItem(Item item) {
        if (player.getInventory().contains(item)) {
            player.setHealth(player.getHealth() + item.getHealth());
            player.setMana(player.getMana() + item.getMana());

            if (player.getInventory().remove(item)) {
                player.getInventory().remove(item);
            }

        }
        System.out.println(player.getHealth());
    }

    /**
     * attacks the specified enemy
     * @param enemy
     * @param isMagic
     * @return output
     */
    public String attackEnemy(Enemy enemy, boolean isMagic) {
        String output = "";
        double damage = 0.0;
        if (isMagic) {
            damage = calculateMagicAttack();
            player.setMana(player.getMana() - 2);
        } else {
            damage = calculateHeroAttack((int) enemy.getDefence());
        }
        enemy.setHealth(enemy.getHealth() - damage);
        output = enemy.getName() + " was attacked for " + damage + " points. HP " + enemy.getHealth();
        return output;

    }

    /**
     * deal damage to the specified enemy
     * @param enemy
     * @return output
     */
    public String dealDamageToPlayer(Enemy enemy) {
        String output = "";
        double damage = (int) calculateEnemyAttack(enemy);
        calculateHeroHealthAfterDamage((int) calculateEnemyAttack(enemy));
        output = " Player was attacked for " + damage + " points";
        return output;
    }

    /**
     * calculates damage to be dealt based on the defence stat
     * @param defence
     * @return damage to be dealt
     */
    public double calculateHeroAttack(int defence) {
        double damage = player.getAttack() + (player.getStrength() / 2) - (defence / 2);
        return damage;
    }

    /**
     *  this method calculates damage to be dealt to the player
     * based on the provided enemy
     * @param enemy
     * @return damage to be dealt to the player
     */
    public double calculateEnemyAttack(Enemy enemy) {
        double damage = enemy.getAttack() + (enemy.getStrength() / 2) - (player.getDefence() / 2);
        return damage;
    }

    /**
     * calculates damage to be dealt for the magic attack 
     * @return
     */
    public double calculateMagicAttack() {
        double damage = player.getAttack() + player.getIntelligence() / 2;
        return damage;
    }

    /**
     * calculates damage done to the player after enemy attack
     * @param damage
     */
    public void calculateHeroHealthAfterDamage(int damage) {

        player.setHealth(player.getHealth() - damage);
    }

    /**
     * equip specified weapon armor
     * @param item
     */
    public void equipArmor(Item item) {
        if (player.getInventory().contains(item) && (!player.getEquippedItems().contains(item)) && !player.isArmorEquipped()) {
            equipArmorWeapon(item);
        }
    }

    /**
     * equip specified weapon
     * @param item
     */
    public void equipWeapon(Item item) {
        if (player.getInventory().contains(item) && !player.getEquippedItems().contains(item) && !player.isWeaponEquipped()) {
            equipArmorWeapon(item);
        }

    }

    /**
     * equips specified item
     * @param item
     */
    public void equipArmorWeapon(Item item) {

        player.setDefence(player.getDefence() + item.getDefence());
        player.setAttack(player.getAttack() + item.getAttack());
        player.setIntelligence(player.getIntelligence() + item.getIntelligence());
        player.setSpeed(player.getSpeed() + item.getSpeed());
        player.setStrength(player.getStrength() + item.getStrength());
        player.setVitality(player.getVitality() + item.getVitality());
        player.getEquippedItems().add(item);

        if (item.getItemType().charAt(0) == 'A') {
            player.setArmorEquipped(true);
        } else {
            player.setWeaponEquipped(true);
        }
        item.setItemEquipped(true);

    }

    /**
     * deeqips specified item
     * @param item
     */
    public void deequipArmorWeapon(Item item) {

        if (player.getInventory().contains(item) && (player.getEquippedItems().contains(item))) {
            player.setDefence(player.getDefence() - item.getDefence());
            player.setAttack(player.getAttack() - item.getAttack());
            player.setIntelligence(player.getIntelligence() - item.getIntelligence());
            player.setSpeed(player.getSpeed() - item.getSpeed());
            player.setStrength(player.getStrength() - item.getStrength());
            player.setVitality(player.getVitality() - item.getVitality());
            player.getEquippedItems().remove(item);
            if (item.getItemType().charAt(0) == 'A') {
                player.setArmorEquipped(false);
            } else {
                player.setWeaponEquipped(false);
            }

            item.setItemEquipped(false);
        }
    }

    private void setUpDungeon(Scanner input) throws SQLException {

        for (int row = 0; row < getDungeon().getNumRows(); row++) {
            String dungeonRow = input.next();

            for (int col = 0; col < dungeonRow.length(); col++) {

                String dungeonString = dungeonRow.substring(col, col + 1);
                char dungChar = dungeonString.charAt(0);

                if (dungChar == 'P') {
                    Position pos = new Position(getDungeon(), row, col);

                    getPlayer().setPosition(pos);
                    getDungeon().updatePlayerPosition(getPlayer());

                }

                if (dungChar == 'I') {
                    Random rand = new Random();
                    int randomNumber = rand.nextInt(2) + 1;
                    while (randomNumber > 0) {

                        Position pos = new Position(getDungeon(), row, col);
                        Item item = db.createRandomItem(pos);

                        getDungeon().getGridSquareByNumbers(row, col).addOccupant(item);
                        randomNumber -= 1;
                    }

                }
                if (dungChar == 'E') {
                    Random rand = new Random();
                    int randomNumber = rand.nextInt(2) + 1;

                    while (randomNumber > 0) {
                        Position pos = new Position(getDungeon(), row, col);
                        Enemy enm = db.createRandomEnemy(pos);
                        enm.setPosition(pos);

                        getDungeon().getGridSquareByNumbers(row, col).addOccupant(enm);
                        randomNumber -= 1;
                    }
                }

                FloorInterior floorInt = FloorInterior.getFloorFromStringRepresentation(dungeonString);
                if (floorInt == FloorInterior.WALKABLE || floorInt == FloorInterior.WALL) {
                    getDungeon().getGridSquareByNumbers(row, col).setFloorInterior(floorInt);
                } else {
                    getDungeon().getGridSquareByNumbers(row, col).setFloorInterior(FloorInterior.WALKABLE);
                }
            }
        }
    }

    private void initialiseDungeonFromFile(String fileName) throws SQLException {
        String row = "";
        String collumn = "";
        int numRows = 0;
        int numCollumns = 0;
        try {
            try (Scanner input = new Scanner(new File(fileName))) {
                System.err.println("Reading file " + fileName);
                row = input.next();
                collumn = input.next();
                System.err.println("Rows: " + row + " Collumn: " + collumn);
                setDungeon(new Dungeon(Integer.parseInt(row), Integer.parseInt(collumn)));
                setUpDungeon(input);
                //setUpOccupants(input);
                input.close();
            }
        } catch (FileNotFoundException e) {
            System.err.println("Unable to find data file '" + fileName + "'");
        }
    }

    /**
     * gets user's input from the command line
     * @return input
     */
    public String getInput() {
        System.out.println(">>Enter command below:");
        String input = "";
        try {
            BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
            input = buff.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return input;
    }

    /**
     * quits game
     */
    public void quitGame() {
        System.exit(0);
    }


    private void checkIfBattle() {
        boolean areThereAnyEnemies = false;
        GridSquare gs = dungeon.getGridSquare(player.getPosition());
        Occupant occs[] = gs.getOccupants();
        for (Occupant occ : occs) {
            if (occ instanceof Enemy) {
                areThereAnyEnemies = true;
            }
        }
        inBattle = true;
    }

    /**
     * checks if there are any enemies at player's grid square
     * @return an array of enemies at player's grid square
     */
    public Object[] getEnemiesNearPlayer() {
        ArrayList<Enemy> enemies = new ArrayList<Enemy>();
        GridSquare gs = dungeon.getGridSquare(player.getPosition());
        Occupant occs[] = gs.getOccupants();
        int count = 0;
        for (Occupant occ : occs) {
            if (occ instanceof Enemy) {
                enemies.add((Enemy) occ);
            }
            count++;
        }

        return enemies.toArray();
    }

    /**
     * displays help
     */
    public void helpDisplay() {
        System.out.println("Enter 'new game', to start a new game, 'quit'\n"
                + " to exit the game. The stats can be viewed by entering 'view\n"
                + " stats', to see the map input 'draw', to move tyep 'move north',\n"
                + "move south', 'move east' or 'move west'. To pick up the item,\n"
                + "type 'pick up item', to access inventory, type 'inventory'\n"
                + "type 'use item plus item number',  to use an item or to equip/deequip weapon/armor'\n"
                + "type 'print hpmana' to view current mana and health\n");
    }

    /**
     * moves player in the specified direction
     * @param dir
     */
    public void movePlayer(MoveDirection dir) {
        if (!player.isIsDead()) {
            if (isPlayerMovePossible(dir)) {
                getDungeon().setPreviousPlayerPos(getPlayer().getPosition());
                Position pos = getPlayer().getPosition().getNewPosition(dir);

                getPlayer().setPosition(pos);
                getDungeon().updatePlayerPosition(getPlayer());

            }
        }
    }

    /**
     * update dungeon
     */
    public void updateDungeon() {
        getDungeon().updatePlayerPosition(player);
    }

    /**
     * player setter
     * @param player
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * checks if player's move in specified direction is possible
     * @param dir
     * @return
     */
    public boolean isPlayerMovePossible(MoveDirection dir) {
        Position currentPos = this.getPlayer().getPosition();
        boolean isPossible = false;
        Position destination = currentPos.getNewPosition(dir);
        GridSquare gridDestination = getDungeon().getGridSquare(destination);

        if (gridDestination != null) {
            gridDestination = this.getDungeon().getGridSquare(destination);

            if (gridDestination.getFloorInterior() == FloorInterior.WALKABLE) {
                isPossible = true;
            }

        }

        if (!isPossible) {
            System.out.println("Cant move in this direction");
        }

        return isPossible;
    }

    /**
     * picks up all items in the player's grid square
     * @return
     */
    public String pickUpItem() {
        String output = "";
        Occupant occupants[];
        occupants = getDungeon().getGridSquare(getPlayer().getPosition()).getOccupants();
        if (occupants.length < 8) {
            for (Occupant occupant : occupants) {
                if (occupant != null && (occupant instanceof Item)) {
                    getPlayer().pickUpItem((Item) occupant, getDungeon().getGridSquare(occupant.getPosition()));
                    output += occupant.getName() + " picked up <br>";
                }
            }
        }
        return output;
    }

    /**
     * @return the dungeon
     */
    public Dungeon getDungeon() {
        return dungeon;
    }

    /**
     * @param dungeon the dungeon to set
     */
    public void setDungeon(Dungeon dungeon) {
        this.dungeon = dungeon;
    }

}
