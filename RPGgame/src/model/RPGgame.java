/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.IOException;
import java.sql.SQLException;



/**
 * a main class for RPG game
 * @author Roman
 */
public class RPGgame {
    
    
      
    public static void main(String[] args) throws IOException, SQLException {
       
        final CommandInterface comm = new CommandInterface();
        comm.controlProgram();
    } 
    
}
