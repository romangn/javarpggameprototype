/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * a class for representing an occupant for a grid square
 * @author Roman
 */
public abstract class Occupant {
    
   private Position position;
   public String name;
   private String description;
   private String strRepresentation;
   
    /**
     * Create an new instance of an occupant
     * @param position
     * @param name
     * @param description
     */
    public Occupant(Position position, String name, String description ) {
       this.position = position;
       this.name = name;
       this.description = description;
   }
   
    /**
     * position getter
     * @return position
     */
    public Position getPosition() {
       return position;
   }
   
    /**
     *name getter
     * @return name
     */
    public String getName() {
       return name;
   }
   
    /**
     *description getter
     * @return description
     */
    public String getDescritption() {
       return description;
   }
   
    /**
     *position setter
     * @param pos
     */
    public void setPosition(Position pos) {
       this.position = pos;
   }
   
    /**
     * returns the string representation of an occupant
     * @return string representation
     */
    public  String getStringRepresentation(){
        return strRepresentation;}



  
}
