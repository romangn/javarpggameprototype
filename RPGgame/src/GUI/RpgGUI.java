/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import javax.swing.JOptionPane;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import model.CommandInterface;
import model.Dungeon;
import model.Enemy;
import model.Item;
import model.MoveDirection;
import model.Player;

/**
 * A gui class that contains, inventory panel, enemies panel, 
 * stats labels, hp and mana status bars. All of those elements are updated once 
 * player has moved or has pressed any button
 * 
 * @author Kira
 */
public class RpgGUI extends javax.swing.JFrame implements KeyListener, ActionListener {

    ImageIcon floorIcon;
    ImageIcon floorWithPlayerIcon;
    ImageIcon wallIcon;
    public CommandInterface comm;
    public MapIcon[][] mapArray;
    public ItemIcon[] inventoryArray;
    public EnemyIcon[] enemiesArray;
    private static final ScheduledExecutorService worker
            = Executors.newSingleThreadScheduledExecutor();

    /**
     * Creates new form rpgGui
     *
     * @param comm game's command iterface
     */
    public RpgGUI(CommandInterface comm) {
        this.comm = comm;
        addKeyListener(this);

    }

    /**
     * Starts up the GUI
     */
    public void startGUI() {
        String wallPath = "images/wall.png";
        String floorPath = "images/floor.png";
        String floorWithPlayerPath = "images/floorWithPlayer.png";

        floorIcon = new ImageIcon(floorPath);
        floorWithPlayerIcon = new ImageIcon(floorWithPlayerPath);
        wallIcon = new ImageIcon(wallPath);

        this.setVisible(true);

        initComponents();
        initMap();
        String name = "";
        while (name.length() < 2) {
            name = JOptionPane.showInputDialog("Please enter your name");
        }
        if (name.length() < 2) {
            name = "player";
        }
        nameLabel.setText("Name:" + name);
        comm.getPlayer().setName(name);
        Object[] classes = {"Mage", "Warrior", "Assasin"};

        int classChoosen = JOptionPane.showOptionDialog(null,
                "Please your your class",
                "Choose a class",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                classes,
                classes[0]);

        if (classChoosen == 0) {
            comm.getPlayer().setMageAttributes();
            classLabel.setText("Class: Mage");
        } else if (classChoosen == 1) {
            comm.getPlayer().setWarriorAttributes();
            classLabel.setText("Class: Warrior");
        } else {
            comm.getPlayer().setAssasinAttributes();
            classLabel.setText("Class: Assasin");
        }
        setGuiBars();
        updateAllGuiComponents();
        updateStatsLabels();
    }

    /**
     * Sets up the enemy panel with enemy icons
     */
    public void setUpEnemiesPanel() {
        GridLayout layout = new GridLayout();
        layout.setRows(1);
        layout.setColumns(2);
        enemiesPanel.setLayout(layout);
        enemiesArray = new EnemyIcon[2];

        for (int i = 0; i < enemiesArray.length; i++) {
            enemiesArray[i] = new EnemyIcon(comm, null);
            ImageIcon itemIcon = new ImageIcon("images/empty.png");
            enemiesArray[i].setMapIconImage(itemIcon);
            enemiesPanel.add(enemiesArray[i]);
            enemiesArray[i].addActionListener(this);
        }

        Object[] enemies = comm.getEnemiesNearPlayer();
        for (int i = 0; i < enemies.length; i++) {
            Enemy it = (Enemy) enemies[i];
            enemiesArray[i] = new EnemyIcon(comm, it);
            ImageIcon itemIcon = new ImageIcon(it.getImagePath());
            enemiesArray[i].setMapIconImage(itemIcon);
        }

    }

    /**
     * Updates Enemies panel
     */
    public void updateEnemiesPanel() {

        Object[] enemies = (Object[]) comm.getEnemiesNearPlayer();
        for (int i = 0; i < enemiesArray.length; i++) {
            enemiesArray[i].setEnemy(null);
            ImageIcon itemIcon = new ImageIcon("images/empty.png");
            enemiesArray[i].setMapIconImage(itemIcon);
            enemiesArray[i].setToolTipText(null);
        }
        enemiesLabel.setText("Current enemies " + enemies.length);
        for (int i = 0; i < enemies.length; i++) {
            Enemy it = (Enemy) enemies[i];
            if (!it.isIsDead()) {
                ImageIcon itemIcon = new ImageIcon(it.getImagePath());
                enemiesArray[i].setMapIconImage(itemIcon);
                enemiesArray[i].setEnemy(it);
                enemiesArray[i].setToolTipText(it.getName() + " - " + it.getDescritption() + " HP" + it.getHealth());
            }
        }
        enemiesPanel.setToolTipText(enemies.length + " Enemies(s)");
        enemiesPanel.updateUI();
    }

    /**
     * Sets up progress bars for HP and Mana
     */
    public void setGuiBars() {

        Player pl = comm.getPlayer();
        hpBar.setMaximum((int) pl.getMaxHealth());
        manaBar.setMaximum((int) pl.getMaxMana());
        updateGuiBars();

    }

    /**
     * Update HP and Mana progress bars
     */
    public void updateGuiBars() {
        Player pl = comm.getPlayer();
        hpBar.setValue((int) pl.getHealth());
        manaBar.setValue((int) pl.getMana());
        hpBar.setToolTipText(pl.getHealth() + "/" + pl.getMaxHealth());
        manaBar.setToolTipText(pl.getMana() + "/" + pl.getMaxMana());
    }

    /**
     *  Sets up inventory panel with enemies icon
     */
    public void setUpIntentoryPanel() {
        
        
        inventoryArray = new ItemIcon[8];

        for (int i = 0; i < inventoryArray.length; i++) {
            inventoryArray[i] = new ItemIcon(comm, null);
            ImageIcon itemIcon = new ImageIcon("images/empty.png");
            inventoryArray[i].setMapIconImage(itemIcon);
            inventoryPanel.add(inventoryArray[i]);
            inventoryArray[i].addActionListener(this);
        }

        Object[] items = (Object[]) comm.getPlayer().getInventory().toArray();

        for (int i = 0; i < items.length; i++) {

            Item it = (Item) items[i];
            inventoryArray[i] = new ItemIcon(comm, it);
            ImageIcon itemIcon = new ImageIcon(it.getImagePath());
            inventoryArray[i].setMapIconImage(itemIcon);

        }

    }

    /**
     * Update stats labels
     */
    public void updateStatsLabels() {
        Player pl = comm.getPlayer();
        vitLabel.setText("Vitality: " + pl.getVitality());
        strLabel.setText("Strength: " + pl.getStrength());
        speedLabel.setText("Speed: " + pl.getSpeed());
        attackLabel.setText("Attack: " + pl.getAttack());
        defenceLabel.setText("Defence: " + pl.getDefence());
        intLabel.setText("Intelligence: " + pl.getIntelligence());

    }

    /**
     * Updaes intentory panel, stats labels, enemies panel and  gui bars
     */
    public void updateAllGuiComponents() {

        updateInventoryPanel();
        updateStatsLabels();
        updateEnemiesPanel();
        updateGuiBars();
        if (comm.getPlayer().isIsDead()) {
            showGameOver();
        }
    }

    /**
     * Updates inventory panel
     */
    public void updateInventoryPanel() {

        Object[] items = (Object[]) comm.getPlayer().getInventory().toArray();
        for (int i = 0; i < inventoryArray.length; i++) {
            inventoryArray[i].setItem(null);
            ImageIcon itemIcon = new ImageIcon("images/empty.png");
            inventoryArray[i].setMapIconImage(itemIcon);
            inventoryArray[i].setToolTipText(null);
        }

        for (int i = 0; i < items.length; i++) {
            Item it = (Item) items[i];
            ImageIcon itemIcon = new ImageIcon(it.getImagePath());
            inventoryArray[i].setMapIconImage(itemIcon);
            inventoryArray[i].setItem(it);
            inventoryArray[i].setToolTipText(it.getName() + " " + it.getDescritption());
        }
        inventoryPanel.setToolTipText(items.length + " Item(s)");

    }

    /**
     * Shows up game over screen
     */
    public void showGameOver() {
        JOptionPane.showMessageDialog(
                null, "You are dead");
    }

    /**
     * Initiates map panel and gui components such as enemies panel, inventory panel and gui bars
     */
    public void initMap() {
        setUpEnemiesPanel();
        setUpIntentoryPanel();
        setGuiBars();
        updateAllGuiComponents();
        Dungeon dung = comm.getDungeon();
        int cols = comm.getDungeon().getNumColumns();
        int rows = comm.getDungeon().getNumRows();
        mapArray = new MapIcon[rows][cols];
        GridLayout layout = new GridLayout();
        layout.setRows(rows);
        layout.setColumns(cols);

        mapPanel.setLayout(layout);
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File("images/floor.png"));
        } catch (IOException e) {
        }
        this.setIconImage(img);

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                mapArray[row][col] = new MapIcon();
                if ("*".equals(dung.getGridSquareByNumbers(row, col).getFloorStringRepresentation())) {

                    mapArray[row][col].setMapIconImage(wallIcon);
                } else {
                    if (dung.getGridSquareByNumbers(row, col).hasPlayer()) {
                        mapArray[row][col].setMapIconImage(floorWithPlayerIcon);
                    } else {
                        if (dung.getGridSquareByNumbers(row, col).checkItems() > 0) {
                            ImageIcon itemIcon = new ImageIcon(dung.getGridSquareByNumbers(row, col).getItemImagePath());
                            mapArray[row][col].setMapIconImage(itemIcon);
                        } else if (dung.getGridSquareByNumbers(row, col).checkEnemies() > 0) {
                            ImageIcon enemyIcon = new ImageIcon(dung.getGridSquareByNumbers(row, col).getEnemyImagePath());
                            mapArray[row][col].setMapIconImage(enemyIcon);
                        } else {
                            mapArray[row][col].setMapIconImage(floorIcon);
                        }
                    }
                }
                mapPanel.add(mapArray[row][col]);
            }
        }

    }

    /**
     * Redraws the elements in the map panel
     */
    public void redrawMap() {

        Dungeon dung = comm.getDungeon();
        int cols = comm.getDungeon().getNumColumns();
        int rows = comm.getDungeon().getNumRows();
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {

                if ("*".equals(dung.getGridSquareByNumbers(row, col).getFloorStringRepresentation())) {

                    mapArray[row][col].setMapIconImage(wallIcon);
                } else {
                    if (dung.getGridSquareByNumbers(row, col).hasPlayer()) {
                        mapArray[row][col].setMapIconImage(floorWithPlayerIcon);
                    } else {
                        if (dung.getGridSquareByNumbers(row, col).checkItems() > 0) {
                            ImageIcon itemIcon = new ImageIcon(dung.getGridSquareByNumbers(row, col).getItemImagePath());
                            mapArray[row][col].setMapIconImage(itemIcon);
                        } else if (dung.getGridSquareByNumbers(row, col).checkEnemies() > 0) {
                            ImageIcon enemyIcon = new ImageIcon(dung.getGridSquareByNumbers(row, col).getEnemyImagePath());
                            mapArray[row][col].setMapIconImage(enemyIcon);
                        } else {
                            mapArray[row][col].setMapIconImage(floorIcon);

                        }
                    }
                }
                mapPanel.add(mapArray[row][col]);
            }
        }
        updateAllGuiComponents();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mapPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        manaBar = new javax.swing.JProgressBar();
        jLabel2 = new javax.swing.JLabel();
        statusLabel = new javax.swing.JLabel();
        inventoryPanel = new javax.swing.JPanel();
        strLabel = new javax.swing.JLabel();
        intLabel = new javax.swing.JLabel();
        speedLabel = new javax.swing.JLabel();
        vitLabel = new javax.swing.JLabel();
        attackLabel = new javax.swing.JLabel();
        defenceLabel = new javax.swing.JLabel();
        enemiesPanel = new javax.swing.JPanel();
        enemiesLabel = new javax.swing.JLabel();
        hpBar = new javax.swing.JProgressBar();
        attackButton = new javax.swing.JButton();
        magicAttackButton = new javax.swing.JButton();
        nameLabel = new javax.swing.JLabel();
        classLabel = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("RPG Game");
        setMinimumSize(new java.awt.Dimension(256, 256));

        mapPanel.setLayout(new java.awt.GridLayout(9, 9));

        jLabel1.setText("HP");

        manaBar.setForeground(new java.awt.Color(0, 0, 153));
        manaBar.setValue(100);

        jLabel2.setText("MN");

        statusLabel.setText("Status");
        statusLabel.setVerticalTextPosition(javax.swing.SwingConstants.TOP);

        inventoryPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        inventoryPanel.setAutoscrolls(true);
        inventoryPanel.setLayout(new java.awt.GridLayout(2, 16));

        strLabel.setText("jLabel3");

        intLabel.setText("jLabel4");

        speedLabel.setText("jLabel5");

        vitLabel.setText("jLabel6");

        attackLabel.setText("jLabel7");

        defenceLabel.setText("jLabel8");

        enemiesPanel.setBackground(new java.awt.Color(255, 255, 255));
        enemiesPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout enemiesPanelLayout = new javax.swing.GroupLayout(enemiesPanel);
        enemiesPanel.setLayout(enemiesPanelLayout);
        enemiesPanelLayout.setHorizontalGroup(
            enemiesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 118, Short.MAX_VALUE)
        );
        enemiesPanelLayout.setVerticalGroup(
            enemiesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 38, Short.MAX_VALUE)
        );

        enemiesLabel.setText("Current enemies");

        hpBar.setForeground(new java.awt.Color(204, 0, 0));

        attackButton.setText("Attack");
        attackButton.setFocusable(false);
        attackButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                attackButtonActionPerformed(evt);
            }
        });

        magicAttackButton.setText("Magic attack");
        magicAttackButton.setFocusable(false);
        magicAttackButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                magicAttackButtonActionPerformed(evt);
            }
        });

        nameLabel.setText("jLabel3");

        classLabel.setText("jLabel4");

        jMenu1.setText("Game");

        jMenuItem1.setText("New game");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Exit");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Help");

        jMenuItem3.setText("Intsructions");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuItem4.setText("About");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(mapPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 563, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(attackLabel, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(defenceLabel, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(enemiesLabel, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(enemiesPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(attackButton, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(magicAttackButton, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(inventoryPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(statusLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(intLabel)
                            .addComponent(strLabel)
                            .addComponent(speedLabel)
                            .addComponent(vitLabel))
                        .addGap(65, 65, 65)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(classLabel)
                            .addComponent(nameLabel)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(manaBar, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
                                    .addComponent(hpBar, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2))))))
                .addContainerGap(54, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(statusLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(inventoryPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(strLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(intLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(speedLabel))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(hpBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(manaBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nameLabel)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(vitLabel)
                            .addComponent(classLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(attackLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(defenceLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(enemiesLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(enemiesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(attackButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(magicAttackButton))
                    .addComponent(mapPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 526, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getAccessibleContext().setAccessibleName("RPG Game");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        try {
            comm.resetDungeon();
        } catch (SQLException ex) {
            Logger.getLogger(RpgGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        Player pl = comm.getPlayer();
        pl.setWarriorAttributes();
        comm.setPlayer(pl);
        setGuiBars();
        updateAllGuiComponents();
        redrawMap();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed

        comm.quitGame();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void attackButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_attackButtonActionPerformed

        String firstEnemy = "";
        String secondEnemy = "";
        if (enemiesArray[0].getEnemy() != null) {
            firstEnemy = comm.attackEnemy(enemiesArray[0].getEnemy(), false) + "<br>" + comm.dealDamageToPlayer(enemiesArray[0].getEnemy());

        }

        if (enemiesArray[1].getEnemy() != null) {
            secondEnemy = comm.attackEnemy(enemiesArray[1].getEnemy(), false) + "<br>" + comm.dealDamageToPlayer(enemiesArray[1].getEnemy());
            ;
        }

        statusLabel.setText("<HTML>" + firstEnemy + secondEnemy + "<HTML>");
        setGuiBars();
        
       
        updateAllGuiComponents();
    }//GEN-LAST:event_attackButtonActionPerformed

    private void magicAttackButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_magicAttackButtonActionPerformed

        String firstEnemy = "";
        String secondEnemy = "";
        if (enemiesArray[0].getEnemy() != null) {
            firstEnemy = comm.attackEnemy(enemiesArray[0].getEnemy(), true) + "<br>" + comm.dealDamageToPlayer(enemiesArray[0].getEnemy());
        }

        if (enemiesArray[1].getEnemy() != null) {
            secondEnemy = comm.attackEnemy(enemiesArray[1].getEnemy(), true) + "<br>" + comm.dealDamageToPlayer(enemiesArray[1].getEnemy());
            
        }
        statusLabel.setText("<HTML>" + firstEnemy + secondEnemy + "<HTML>");
        updateAllGuiComponents();
    }//GEN-LAST:event_magicAttackButtonActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        JOptionPane.showMessageDialog(
                null, "W,A,S,D to move \n"
                + "E to pick up items \n"
                + "Click on an item in inventory panel to use it or to equip/deequip \n"
                + "Use an Attack button to perform an attack\n"
                + "Use an Magic Attack button to perform a Magic attack\n");
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        JOptionPane.showMessageDialog(
                null, "Roman Gubaidullin 0950891 Rpg game");
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RpgGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RpgGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RpgGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RpgGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton attackButton;
    private javax.swing.JLabel attackLabel;
    private javax.swing.JLabel classLabel;
    private javax.swing.JLabel defenceLabel;
    private javax.swing.JLabel enemiesLabel;
    private javax.swing.JPanel enemiesPanel;
    private javax.swing.JProgressBar hpBar;
    private javax.swing.JLabel intLabel;
    private javax.swing.JPanel inventoryPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JButton magicAttackButton;
    private javax.swing.JProgressBar manaBar;
    private javax.swing.JPanel mapPanel;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JLabel speedLabel;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JLabel strLabel;
    private javax.swing.JLabel vitLabel;
    // End of variables declaration//GEN-END:variables

    /**
     *
     * @param e
     */
    @Override
    public void keyPressed(KeyEvent e) {

        switch (e.getKeyCode()) {
            case KeyEvent.VK_D: {
                comm.movePlayer(MoveDirection.EAST);
                redrawMap();

                break;
            }
            case KeyEvent.VK_A: {
                comm.movePlayer(MoveDirection.WEST);
                redrawMap();

                break;
            }
            case KeyEvent.VK_W: {
                comm.movePlayer(MoveDirection.NORTH);
                redrawMap();

                break;
            }
            case KeyEvent.VK_S: {
                comm.movePlayer(MoveDirection.SOUTH);
                redrawMap();

                break;
            }
            case KeyEvent.VK_E: {
                statusLabel.setText("<HTML>" + comm.pickUpItem() + "<HTML>");
                redrawMap();

                break;
            }

        }

    }

    @Override
    public void keyTyped(KeyEvent ke) {

    }

    @Override
    public void keyReleased(KeyEvent ke) {

    }

    /**
     * Updates gui components a button is pressed on the inventory panel
     * @param ae action event from the button pressed on the inventory panel
     */
    @Override
    public void actionPerformed(ActionEvent ae) {

        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        updateAllGuiComponents();
                    }
                },
                2
        );

    }

}
