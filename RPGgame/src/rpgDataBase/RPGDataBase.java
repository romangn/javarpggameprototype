/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpgDataBase;

import java.sql.*;
import java.util.Random;
import model.Enemy;
import model.Item;
import model.Position;

/**
 * Class for connection to the database that stores the information about
 * different items and enemies
 *
 * @author Roman
 */
public class RPGDataBase {

    private Connection connect = null;
    private Statement statement = null;
    private ResultSet resultSet = null;

    /**
     * Creates  a new instance of the RPGDataBase
     */
    public RPGDataBase() {
        // TODO code application logic here
        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        }

        try {
            this.connect = DriverManager.getConnection("jdbc:derby://localhost:1527/rpgDB", "rpg", " ");
            Statement stmt = this.connect.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT * FROM APP.ENEMY");

            while (rs.next()) {
                String s = rs.getString("NAME");
                String n = rs.getString("DESCRIPTION");
                System.out.println(s + "   " + n);
            }
        } catch (SQLException e) {
            System.err.println(e);
        }

    }

    /**
     * Gets data from the database and creates an enemy random properties from
     * the
     *
     * @param pos
     * @return
     * @throws SQLException
     */
    public Enemy createRandomEnemy(Position pos) throws SQLException {
        Statement stmt = this.connect.createStatement();
        Random rand = new Random();
        int counter = 0;
        int randomNumber;
        Enemy randomEnemy;
        ResultSet rs = stmt.executeQuery("SELECT * FROM APP.ENEMY ");
        while (rs.next()) {
            counter += 1;
        }
        randomNumber = rand.nextInt(counter) + 1;

        rs = stmt.executeQuery("SELECT * FROM APP.ENEMY WHERE ENEMYID=" + randomNumber);
        rs.next();
        String name = rs.getString("NAME");
        String description = rs.getString("DESCRIPTION");
        int str = rs.getInt("STRENGTH");
        int vit = rs.getInt("VITALITY");
        int speed = rs.getInt("SPEED");
        int intel = rs.getInt("INTELLIGENCE");
        double hp = rs.getDouble("HEALTH");
        double attack = rs.getDouble("ATTACK");
        double def = rs.getDouble("DEFENCE");
        String imagePath = rs.getString("IMAGEPATH");

        randomEnemy = new Enemy(pos, name, description);
        randomEnemy.setAttack(attack);
        randomEnemy.setDefence(def);
        randomEnemy.setHealth(hp);
        randomEnemy.setIntelligence(intel);
        randomEnemy.setSpeed(speed);
        randomEnemy.setStrength(str);
        randomEnemy.setVitality(vit);
        randomEnemy.setImagePath(imagePath);
        randomEnemy.setEnemyID(randomNumber);

        return randomEnemy;

    }

    /**
     * Gets data from the database and creates an item with random properties
     * from the database
     *
     * @param pos position of the item
     * @return random item
     * @throws SQLException
     */
    public Item createRandomItem(Position pos) throws SQLException {
        int counter = 0;
        Statement stmt = this.connect.createStatement();
        Random rand = new Random();
        int randomNumber = rand.nextInt(11) + 1;
        Item randomItem;
        ResultSet rs = stmt.executeQuery("SELECT * FROM APP.ITEM ");
        while (rs.next()) {
            counter += 1;
        }
        randomNumber = rand.nextInt(counter) + 1;

        rs = stmt.executeQuery("SELECT * FROM APP.ITEM WHERE ITEMID=" + randomNumber);
        rs.next();

        String name = rs.getString("NAME");
        String description = rs.getString("DESCRIPTION");
        int str = rs.getInt("STRENGTH");
        int vit = rs.getInt("VITALITY");
        int speed = rs.getInt("SPEED");
        int intel = rs.getInt("INTELLIGENCE");
        double hp = rs.getDouble("HEALTH");
        double mana = rs.getDouble("MANA");
        double attack = rs.getDouble("ATTACK");
        double def = rs.getDouble("DEFENCE");
        String imagePath = rs.getString("IMAGEPATH");
        String itemType = rs.getString("ITEMTYPE");

        randomItem = new Item(pos, name, description);
        randomItem.setAttack(attack);
        randomItem.setDefence(def);
        randomItem.setHealth(hp);
        randomItem.setIntelligence(intel);
        randomItem.setSpeed(speed);
        randomItem.setStrength(str);
        randomItem.setVitality(vit);
        randomItem.setImagePath(imagePath);
        randomItem.setItemType(itemType);

        return randomItem;
    }

}
